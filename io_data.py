import json


def save(variable):
    to_json = {"high_score": variable}
    with open("saves.json", "w") as f:
        json.dump(to_json, f)


def load():
    try:
        with open("saves.json", "r") as f:
            from_json = json.load(f)
        variable = from_json["high_score"]
    except FileNotFoundError:
        variable = 0
    return variable
