from random import uniform

from kivy.app import App
from kivy.core.image import Image
from kivy.uix.widget import Widget
from kivy.core.text import LabelBase
from kivy.clock import Clock
from kivy.uix.image import Image as ImageWidget
from kivy.core.audio import SoundLoader
from kivy.properties import AliasProperty, ListProperty, NumericProperty, ObjectProperty

from io_data import save, load


class BaseWidget(Widget):
    def load_tileable(self, name):
        t = Image(f"images/{name}.png").texture
        t.wrap = "repeat"
        setattr(self, f"tx_{name}", t)


class Background(BaseWidget):
    tx_floor = ObjectProperty(None)
    tx_grass = ObjectProperty(None)
    tx_cloud = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(Background, self).__init__(**kwargs)
        for name in ("floor", "grass", "cloud"):
            self.load_tileable(name)

    def set_background_size(self, tx):
        tx.uvsize = (self.width / tx.width, -1)

    def on_size(self, *args):
        for tx in (self.tx_floor, self.tx_grass, self.tx_cloud):
            self.set_background_size(tx)

    def set_background_uv(self, name, val):
        t = getattr(self, name)
        t.uvpos = ((t.uvpos[0] + val) % self.width, t.uvpos[1])
        self.property(name).dispatch(self)

    def update(self, nap):
        self.set_background_uv("tx_cloud", 0.1 * nap)
        self.set_background_uv("tx_grass", 0)
        self.set_background_uv("tx_floor", 0)


class Pipe(BaseWidget):
    FLOOR = 96
    PCAP_HEIGHT = 26
    PIPE_GAP = 200

    tx_pipe = ObjectProperty(None)
    tx_pcap = ObjectProperty(None)
    ratio = NumericProperty(0.5)
    lower_len = NumericProperty(0)
    lower_coords = ListProperty((0, 0, 1, 0, 1, 1, 0, 1))
    upper_len = NumericProperty(0)
    upper_coords = ListProperty((0, 0, 1, 0, 1, 1, 0, 1))
    upper_y = AliasProperty(
        lambda self: self.height - self.upper_len, None, bind=["height", "upper_len"]
    )

    def __init__(self, **kwargs):
        super(Pipe, self).__init__(**kwargs)
        for name in ("pipe", "pcap"):
            self.load_tileable(name)

    def set_coords(self, coords, length):
        length /= 16  # height of the texture
        coords[5:] = (length, 0, length)  # set the last 3 items

    def on_size(self, *args):
        pipes_length = self.height - (Pipe.FLOOR + Pipe.PIPE_GAP + 2 * Pipe.PCAP_HEIGHT)
        self.lower_len = self.ratio * pipes_length
        self.upper_len = pipes_length - self.lower_len
        self.set_coords(self.lower_coords, self.lower_len)
        self.set_coords(self.upper_coords, self.upper_len)
        self.bind(ratio=self.on_size)


class Bird(ImageWidget):
    ACCEL_FALL = 0.25
    ACCEL_JUMP = 5

    speed = NumericProperty(0)
    angle = AliasProperty(lambda self: 5 * self.speed, None, bind=["speed"])

    def gravity_on(self, height):
        self.pos_hint.pop("center_y", None)
        self.center_y = 0.6 * height

    def bump(self):
        self.speed = Bird.ACCEL_JUMP

    def update(self, nap):
        self.speed -= Bird.ACCEL_FALL
        self.y += self.speed


class ScoreWidget(Widget):
    pass


class PtashechkaApp(App):
    score = NumericProperty(0)
    high_score = NumericProperty(0)
    counter = NumericProperty(0)
    pipes = []
    playing = False
    sound_game_over = SoundLoader.load("sounds/game_over.wav")
    sound_flutter = SoundLoader.load("sounds/flutter.wav")
    sound_main_theme = SoundLoader.load("sounds/birds_singing.wav")

    def on_start(self):
        self.spacing = 0.5 * self.root.width
        self.background = self.root.ids.background
        self.bird = self.root.ids.bird
        self.sound_main_theme.loop = True
        self.sound_main_theme.play()
        self.high_score = load()
        self.root.ids.high_score.text = f"high score [size=70]{self.high_score}[/size]"
        Clock.schedule_interval(self.update, 1 / 60)
        self.background.on_touch_down = self.user_action

    def update(self, nap):
        self.background.update(nap)
        if not self.playing:
            return
        self.bird.update(nap)

        for pipe in self.pipes:
            pipe.x -= 96 * nap
            if pipe.x <= -64:
                pipe.x += 4 * self.spacing
                pipe.ratio = uniform(0.25, 0.75)

        self.counter += 1
        if self.counter % 60 == 0:
            self.score += 1

        if self.test_game_over():
            self.playing = False
            self.sound_game_over.play()
            for pipe in self.pipes:
                self.root.remove_widget(pipe)
            self.game_over()

    def spawn_pipes(self):
        for pipe in self.pipes:
            self.root.remove_widget(pipe)
        self.pipes = []
        for i in range(4):
            pipe = Pipe(x=self.root.width + (self.spacing * i))
            pipe.ratio = uniform(0.25, 0.75)
            self.root.add_widget(pipe)
            self.pipes.append(pipe)

    def user_action(self, *args):
        if not self.playing:
            self.playing = True
            self.bird.gravity_on(self.root.height)
            self.spawn_pipes()
            self.root.ids.title.color = (0, 0, 0, 0)
            self.root.ids.high_score.color = (0, 0, 0, 0)
            self.root.add_widget(ScoreWidget())
        self.bird.bump()
        self.sound_flutter.play()

    def test_game_over(self):
        screen_height = self.root.height
        if self.bird.y < 90 or self.bird.y > screen_height - 50:
            return True

        for pipe in self.pipes:
            if not pipe.collide_widget(self.bird):
                continue

            if self.bird.y < pipe.lower_len + 122 or self.bird.y > screen_height - (
                pipe.upper_len + 106
            ):
                return True

        return False

    def game_over(self):
        if self.high_score < self.score:
            self.high_score = self.score
        save(self.high_score)
        self.score = 0
        self.root.ids.high_score.text = f"high score [size=70]{self.high_score}[/size]"
        self.root.ids.title.color = (0, 0, 0, 1)
        self.root.ids.high_score.color = (0, 0, 0, 1)
        self.bird.pos_hint = {"center_x": 0.38, "center_y": 0.635}
        self.bird.speed = 0


if __name__ == "__main__":
    LabelBase.register(
        name="Komika",
        fn_regular="fonts/Komika_display.ttf",
    )
    PtashechkaApp().run()
